//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_json_jsonResource_h
#define smtk_session_opencascade_json_jsonResource_h

#include "smtk/session/opencascade/Exports.h"

#include "smtk/session/opencascade/Resource.h"

#include "nlohmann/json.hpp"

namespace smtk
{
namespace session
{
namespace opencascade
{

SMTKOPENCASCADESESSION_EXPORT void to_json(nlohmann::json& j, const smtk::session::opencascade::Resource::Ptr& resource);

SMTKOPENCASCADESESSION_EXPORT void from_json(const nlohmann::json& j, smtk::session::opencascade::Resource::Ptr& resource);

} // namespace opencascade
} // namespace session
} // namespace smtk

#endif // smtk_session_opencascade_json_jsonResource_h
