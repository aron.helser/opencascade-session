//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/opencascade/Registrar.h"

#include "smtk/session/opencascade/IconConstructor.h"
#include "smtk/session/opencascade/Resource.h"
#include "smtk/session/opencascade/operators/CreateBox.h"
#include "smtk/session/opencascade/operators/CreateResource.h"
#include "smtk/session/opencascade/operators/Cut.h"
#include "smtk/session/opencascade/operators/Import.h"
#include "smtk/session/opencascade/operators/Read.h"
#include "smtk/session/opencascade/operators/Write.h"

#include "smtk/operation/groups/CreatorGroup.h"
#include "smtk/operation/groups/ImporterGroup.h"
#include "smtk/operation/groups/ReaderGroup.h"
#include "smtk/operation/groups/WriterGroup.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

namespace
{
using OperationList = std::tuple<CreateBox, CreateResource, Cut, Import, Read, Write>;
}

void Registrar::registerTo(const smtk::resource::Manager::Ptr& resourceManager)
{
  // resource type to a manager
  resourceManager->registerResource<smtk::session::opencascade::Resource>(read, write);
}

void Registrar::registerTo(const smtk::operation::Manager::Ptr& operationManager)
{
  // Register operations to the operation manager
  operationManager->registerOperations<OperationList>();

  smtk::operation::CreatorGroup(operationManager)
    .registerOperation<smtk::session::opencascade::Resource,
      smtk::session::opencascade::CreateResource>();
  smtk::operation::ImporterGroup(operationManager)
    .registerOperation<smtk::session::opencascade::Resource, smtk::session::opencascade::Import>();
  smtk::operation::ReaderGroup(operationManager)
    .registerOperation<smtk::session::opencascade::Resource, smtk::session::opencascade::Read>();
  smtk::operation::WriterGroup(operationManager)
    .registerOperation<smtk::session::opencascade::Resource, smtk::session::opencascade::Write>();
}

void Registrar::registerTo(const smtk::view::Manager::Ptr& viewManager)
{
  viewManager->objectIcons().registerIconConstructor<Resource>(IconConstructor());
}

void Registrar::unregisterFrom(const smtk::resource::Manager::Ptr& resourceManager)
{
  resourceManager->unregisterResource<smtk::session::opencascade::Resource>();
}

void Registrar::unregisterFrom(const smtk::operation::Manager::Ptr& operationManager)
{
  smtk::operation::CreatorGroup(operationManager)
    .unregisterOperation<smtk::session::opencascade::CreateResource>();
  smtk::operation::ImporterGroup(operationManager)
    .unregisterOperation<smtk::session::opencascade::Import>();
  smtk::operation::ReaderGroup(operationManager)
    .unregisterOperation<smtk::session::opencascade::Read>();
  smtk::operation::WriterGroup(operationManager)
    .unregisterOperation<smtk::session::opencascade::Write>();

  operationManager->unregisterOperations<OperationList>();
}

void Registrar::unregisterFrom(const smtk::view::Manager::Ptr& viewManager)
{
  (void)viewManager;
  // TODO: Unregister icons?
}
}
}
}
