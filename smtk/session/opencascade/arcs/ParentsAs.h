//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_ParentsAs_h
#define smtk_session_opencascade_ParentsAs_h

#include "smtk/graph/Resource.h"
#include "smtk/session/opencascade/Shape.h"

#include "smtk/common/CompilerInformation.h"

#include "TopoDS_Shape.hxx"
#include <TopExp.hxx>

namespace smtk
{
namespace session
{
namespace opencascade
{

template<typename XToType>
class ParentsAs
{
public:
  typedef Shape FromType;
  typedef XToType ToType;
  typedef std::vector<std::reference_wrapper<const ToType> > Container;

  ParentsAs(const FromType& from)
    : m_from(from)
  {
  }

  const FromType& from() const { return m_from; }
  Container to() const
  {
    Container container;
    this->visit([&container](const ToType& toType) {
      container.push_back(std::cref(toType));
      return false;
    });
    return container;
  }

  template<bool IsConst>
  class Iterator
  {
    const Shape* m_parent;
    Standard_Integer m_index, m_extent;
    TopTools_IndexedDataMapOfShapeListOfShape m_map;

    Iterator(const Shape* parent, bool isEnd);

    friend class ParentsAs;

  public:
    Iterator() = default;

    Iterator& operator++();
    Iterator operator++(int);
    typename std::conditional<IsConst, const XToType&, XToType&>::type operator*();
    typename std::conditional<IsConst, const XToType*, XToType*>::type operator->();

    bool operator==(const Iterator& other) const;
    bool operator!=(const Iterator& other) const { return !(*this == other); }
  };

  using iterator = Iterator<false>;
  using const_iterator = Iterator<true>;

  iterator begin() { return iterator(&m_from, false); }
  iterator end() { return iterator(&m_from, true); }
  const_iterator begin() const { return const_iterator(&m_from, false); }
  const_iterator end() const { return const_iterator(&m_from, true); }

  bool visit(const std::function<bool(const ToType&)>&) const;

  bool visit(const std::function<bool(ToType&)>&);

  template<typename SelfType>
  class API
  {
  protected:
    SelfType& self(const typename SelfType::FromType& lhs) const
    {
      auto& arcs = std::static_pointer_cast<smtk::graph::ResourceBase>(lhs.resource())->arcs();
      if (!arcs.template contains<SelfType>(lhs.id()))
      {
        arcs.template emplace<SelfType>(lhs.id(), lhs);
      }
      return arcs.template get<SelfType>().at(lhs.id());
    }

  public:
    const SelfType& get(const typename SelfType::FromType& lhs) const { return self(lhs); }
    SelfType& get(const typename SelfType::FromType& lhs) { return self(lhs); }

    bool contains(const typename SelfType::FromType& lhs) const
    {
      return std::static_pointer_cast<smtk::graph::ResourceBase>(lhs.resource())
        ->arcs()
        .template get<SelfType>()
        .contains(lhs.id());
    }

    bool visit(const FromType& lhs, std::function<bool(const typename SelfType::ToType&)> fn) const
    {
      return self(lhs).visit(fn);
    }

    bool visit(const FromType& lhs, std::function<bool(typename SelfType::ToType&)> fn)
    {
      return self(lhs).visit(fn);
    }
  };

private:
  const FromType& m_from;
};

typedef ParentsAs<Shape> Parents;
}
}
}

#endif
