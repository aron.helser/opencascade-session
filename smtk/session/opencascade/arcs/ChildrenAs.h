//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_ChildrenAs_h
#define smtk_session_opencascade_ChildrenAs_h

#include "smtk/graph/Resource.h"
#include "smtk/session/opencascade/Shape.h"

#include "smtk/common/CompilerInformation.h"

#include "TopoDS_Iterator.hxx"
#include "TopoDS_Shape.hxx"

namespace smtk
{
namespace session
{
namespace opencascade
{

template<typename XToType>
class ChildrenAs
{
public:
  typedef Shape FromType;
  typedef XToType ToType;
  typedef std::vector<std::reference_wrapper<const ToType> > Container;

  template<bool IsConst>
  class Iterator
  {
    const Shape* m_parent;
    TopoDS_Shape* m_store = nullptr;
    std::unique_ptr<TopoDS_Iterator> m_it;
    bool m_isEnd = true;

    // Construct an iterator that will iterate over all nodes
    Iterator(const Shape* parent, bool isEnd);
    // Construct a proxy iterator that only stores one valid node
    // used for the post-increment result.
    Iterator(const Shape* parent, TopoDS_Shape& shape);

    friend class ChildrenAs;

  public:
    Iterator() = default;

    Iterator& operator++();
    Iterator operator++(int);
    typename std::conditional<IsConst, const XToType&, XToType&>::type operator*();
    typename std::conditional<IsConst, const XToType*, XToType*>::type operator->();

    bool operator==(const Iterator& other) const;
    bool operator!=(const Iterator& other) const { return !(*this == other); }
  };

  using iterator = Iterator<false>;
  using const_iterator = Iterator<true>;

  ChildrenAs(const FromType& from)
    : m_from(from)
  {
  }

  const FromType& from() const { return m_from; }
  Container to() const
  {
    Container container;
    this->visit([&container](const ToType& toType) {
      container.push_back(std::cref(toType));
      return false;
    });
    return container;
  };

  iterator begin() { return iterator(&m_from, false); }
  iterator end() { return iterator(&m_from, true); }
  const_iterator begin() const { return const_iterator(&m_from, false); }
  const_iterator end() const { return const_iterator(&m_from, true); }

  bool visit(const std::function<bool(const ToType&)>& fn) const;

  bool visit(const std::function<bool(ToType&)>& fn);

  template<typename SelfType>
  class API
  {
  protected:
    const SelfType& self(const typename SelfType::FromType& lhs) const
    {
      auto& arcs = std::static_pointer_cast<smtk::graph::ResourceBase>(lhs.resource())->arcs();
      if (!arcs.template contains<SelfType>(lhs.id()))
      {
        arcs.template emplace<SelfType>(lhs.id(), lhs);
      }
      return arcs.template get<SelfType>().at(lhs.id());
    }

    SelfType& self(const typename SelfType::FromType& lhs)
    {
      auto& arcs = std::static_pointer_cast<smtk::graph::ResourceBase>(lhs.resource())->arcs();
      if (!arcs.template contains<SelfType>(lhs.id()))
      {
        arcs.template emplace<SelfType>(lhs.id(), lhs);
      }
      return arcs.template get<SelfType>().at(lhs.id());
    }

  public:
    const SelfType& get(const typename SelfType::FromType& lhs) const { return self(lhs); }

    SelfType& get(const typename SelfType::FromType& lhs) { return self(lhs); }

    bool contains(const typename SelfType::FromType& lhs) const
    {
      return std::static_pointer_cast<smtk::graph::ResourceBase>(lhs.resource())
        ->arcs()
        .template get<SelfType>()
        .contains(lhs.id());
    }

    bool visit(const FromType& lhs,
      const std::function<bool(const typename SelfType::ToType&)>& fn) const
    {
      return self(lhs).visit(fn);
    }

    bool visit(const FromType& lhs, const std::function<bool(typename SelfType::ToType&)>& fn)
    {
      return self(lhs).visit(fn);
    }
  };

protected:
  const FromType& m_from;
};

typedef ChildrenAs<Shape> Children;
}
}
}

#endif
