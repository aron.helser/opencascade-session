//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_ParentsAs_txx
#define smtk_session_opencascade_ParentsAs_txx

#include "smtk/session/opencascade/arcs/ParentsAs.h"

#include "smtk/session/opencascade/Resource.h"
#include "smtk/session/opencascade/Session.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

template<typename XToType>
template<bool IsConst>
ParentsAs<XToType>::Iterator<IsConst>::Iterator(const Shape* parent, bool isEnd)
  : m_parent(parent)
{
  auto resource = static_pointer_cast<smtk::session::opencascade::Resource>(m_parent->resource());
  auto session = resource->session();

  if (auto shape = m_parent->data())
  {
    auto shapeType = shape->ShapeType();
    if (shapeType == TopAbs_COMPOUND)
    {
      m_index = 0;
      m_extent = 0;
    }
    else
    {
      if (isEnd)
      {
        m_extent = -1;
        m_index = -1;
      }
      else
      {
        TopExp::MapShapesAndAncestors(resource->compound(),
          shapeType,
          static_cast<TopAbs_ShapeEnum>(static_cast<int>(shapeType) - 1),
          m_map);
        m_extent = m_map.Extent();
        m_index = 1;
      }
    }
  }
}

template<typename XToType>
template<bool IsConst>
typename ParentsAs<XToType>::template Iterator<IsConst>&
ParentsAs<XToType>::Iterator<IsConst>::operator++()
{
  auto resource = static_pointer_cast<smtk::session::opencascade::Resource>(m_parent->resource());
  auto session = resource->session();
  XToType* node = nullptr;
  for (; !node && m_index < m_extent; ++m_index)
  {
    auto uid = session->findID(m_map.FindKey(m_index));
    if (!uid)
    {
      continue;
    }
    node = dynamic_cast<XToType*>(resource->find(uid).get());
  }
  return *this;
}

template<typename XToType>
template<bool IsConst>
typename ParentsAs<XToType>::template Iterator<IsConst>
ParentsAs<XToType>::Iterator<IsConst>::operator++(int)
{
  auto tmp = m_index;
  ++(*this);
  return tmp;
}

template<typename XToType>
template<bool IsConst>
typename std::conditional<IsConst, const XToType&, XToType&>::type
ParentsAs<XToType>::Iterator<IsConst>::operator*()
{
  if (m_index == m_extent)
  {
    throw std::out_of_range("ParentAs<>::Iterator: Attempting to dereference end iterator.");
  }
  auto resource = static_pointer_cast<smtk::session::opencascade::Resource>(m_parent->resource());
  auto session = resource->session();
  auto uid = session->findID(m_map.FindKey(m_index));
  return *dynamic_cast<XToType*>(resource->find(uid).get());
}

template<typename XToType>
template<bool IsConst>
typename std::conditional<IsConst, const XToType*, XToType*>::type
ParentsAs<XToType>::Iterator<IsConst>::operator->()
{
  return &(this->operator*());
}

template<typename XToType>
template<bool IsConst>
bool ParentsAs<XToType>::Iterator<IsConst>::operator==(const Iterator& other) const
{
  assert(m_parent == other.m_parent);
  if (m_parent == other.m_parent)
  {
    auto thisIndex = m_index == m_extent ? -1 : m_index;
    auto otherIndex = other.m_index == other.m_extent ? -1 : other.m_index;
    return thisIndex == otherIndex;
  }
  else
  {
    return false;
  }
}

template<typename XToType>
bool ParentsAs<XToType>::visit(
  const std::function<bool(const typename ParentsAs<XToType>::ToType&)>& fn) const
{
  auto resource = static_pointer_cast<smtk::session::opencascade::Resource>(m_from.resource());
  auto session = resource->session();

  if (auto shape = m_from.data())
  {
    // If our shape is a compound, it has no parent
    auto shapeType = shape->ShapeType();
    if (shapeType == TopAbs_COMPOUND)
    {
      return true;
    }

    TopTools_IndexedDataMapOfShapeListOfShape map;
    TopExp::MapShapesAndAncestors(resource->compound(),
      shapeType,
      static_cast<TopAbs_ShapeEnum>(static_cast<int>(shapeType) - 1),
      map);

    const TopTools_ListOfShape& parents = map.FindFromKey(*shape);

    for (const TopoDS_Shape& parent : parents)
    {
      auto uid = session->findID(parent);
      if (!uid)
      {
        continue;
      }
      auto node = dynamic_cast<ToType*>(resource->find(uid).get());
      if (!node)
      {
        continue;
      }
      if (fn(*node))
      {
        return true;
      }
    }
  }
  return false;
}

template<typename XToType>
bool ParentsAs<XToType>::visit(const std::function<bool(typename ParentsAs<XToType>::ToType&)>& fn)
{
  auto resource = static_pointer_cast<smtk::session::opencascade::Resource>(m_from.resource());
  auto session = resource->session();

  if (auto shape = m_from.data())
  {
    // If our shape is a compound, it has no parent
    auto shapeType = shape->ShapeType();
    if (shapeType == TopAbs_COMPOUND)
    {
      return true;
    }

    TopTools_IndexedDataMapOfShapeListOfShape map;
    TopExp::MapShapesAndAncestors(
      *shape, shapeType, static_cast<TopAbs_ShapeEnum>(static_cast<int>(shapeType) - 1), map);

    Standard_Integer i, nE = map.Extent();
    for (i = 1; i < nE; i++)
    {
      auto uid = session->findID(map.FindKey(i));
      if (!uid)
      {
        continue;
      }
      auto node = dynamic_cast<ToType*>(resource->find(uid).get());
      if (!node)
      {
        continue;
      }
      if (fn(*node))
      {
        return true;
      }
    }
  }
  return false;
}
}
}
}

#endif
