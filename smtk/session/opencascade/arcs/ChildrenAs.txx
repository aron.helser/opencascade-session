//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_ChildrenAs_txx
#define smtk_session_opencascade_ChildrenAs_txx

#include "smtk/session/opencascade/arcs/ChildrenAs.h"

#include "smtk/session/opencascade/Resource.h"
#include "smtk/session/opencascade/Session.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

template<typename XToType>
template<bool IsConst>
ChildrenAs<XToType>::Iterator<IsConst>::Iterator(const Shape* parent, bool isEnd)
  : m_parent(parent)
  , m_it(new TopoDS_Iterator)
  , m_isEnd(isEnd)
{
  if (!m_isEnd)
  {
    m_it->Initialize(*(m_parent->data()));
    if (m_it->More())
    {
      auto resource = static_pointer_cast<Resource>(m_parent->resource());
      auto session = resource->session();
      // Find the first node in the shape iterator
      auto uid = session->findID(m_it->Value());
      while (!uid && m_it->More())
      {
        m_it->Next();
        uid = session->findID(m_it->Value());
      }
      // If there was no valid uid, then there are no children and this is the end.
      if (!uid)
      {
        m_isEnd = true;
      }
      // Found a valid ID
      else
      {
        // Check if the first found node is of XToType
        auto* node = dynamic_cast<XToType*>(resource->find(uid).get());
        if (!node)
        {
          // The first node is not of XToType, seek to the first node that is.
          this->operator++();
        }
      }
    }
    else
    {
      m_isEnd = true;
    }
  }
}

template<typename XToType>
template<bool IsConst>
ChildrenAs<XToType>::Iterator<IsConst>::Iterator(const Shape* parent, TopoDS_Shape& shape)
  : m_parent(parent)
  , m_store(&shape)
  , m_isEnd(false)
{
}

template<typename XToType>
template<bool IsConst>
typename ChildrenAs<XToType>::template Iterator<IsConst>&
ChildrenAs<XToType>::Iterator<IsConst>::operator++()
{
  if (!m_isEnd)
  {
    if (m_it)
    {
      auto resource = static_pointer_cast<Resource>(m_parent->resource());
      auto session = resource->session();
      XToType* node = nullptr;
      for (; !node && m_it->More(); m_it->Next())
      {
        auto uid = session->findID(m_it->Value());
        if (!uid)
        {
          continue;
        }
        node = dynamic_cast<XToType*>(resource->find(uid).get());
      }
      m_isEnd = !(m_it->More());
    }
    else
    {
      m_isEnd = true;
      if (m_store)
      {
        m_store = nullptr;
      }
    }
  }
#ifndef NDEBUG
  else
  {
    throw std::out_of_range("ChildrenAs<>::Iterator: Attempting to increment past end");
  }
#endif
  return *this;
}

template<typename XToType>
template<bool IsConst>
typename ChildrenAs<XToType>::template Iterator<IsConst>
ChildrenAs<XToType>::Iterator<IsConst>::operator++(int)
{
  if (!m_isEnd && m_it)
  {
    Iterator store(m_parent, m_it->Value());
    this->operator++();
    return store;
  }
  else
  {
    return Iterator();
  }
}

template<typename XToType>
template<bool IsConst>
typename std::conditional<IsConst, const XToType&, XToType&>::type
ChildrenAs<XToType>::Iterator<IsConst>::operator*()
{
  if (m_isEnd)
  {
    throw std::out_of_range("ChildrenAs<>::Iterator: Attempting to dereference end iterator.");
  }
  if (m_it && m_it->More())
  {
    auto resource = static_pointer_cast<Resource>(m_parent->resource());
    auto session = resource->session();
    auto uid = session->findID(m_it->Value());
    assert(uid);
    auto* node = dynamic_cast<XToType*>(resource->find(uid).get());
#ifndef NDEBUG
    if (node)
    {
#endif
      return *node;
#ifndef NDEBUG
    }
    else
    {
      throw std::out_of_range(
        "ChildrenAs<>::Iterator: Attempting to derefernce node of wrong type.");
    }
#endif
  }
}

template<typename XToType>
template<bool IsConst>
typename std::conditional<IsConst, const XToType*, XToType*>::type
ChildrenAs<XToType>::Iterator<IsConst>::operator->()
{
  return &(this->operator*());
}

template<typename XToType>
template<bool IsConst>
bool ChildrenAs<XToType>::Iterator<IsConst>::operator==(const Iterator& other) const
{
  assert(this->m_parent == other.m_parent);
  if (this->m_parent == other.m_parent)
  {
    // First check if they are both at the end of the iteration.
    if (m_isEnd && other.m_isEnd)
    {
      return true;
    }
    // If only one of the iterators is the end they are not equal
    else if (m_isEnd || other.m_isEnd)
    {
      return false;
    }
    // Finally, check if the current shapes are the same.
    else
    {
      const TopoDS_Shape* currShape = (m_it ? &m_it->Value() : m_store);
      const TopoDS_Shape* otherShape = (m_it ? &other.m_it->Value() : other.m_store);
      return currShape == otherShape;
    }
  }
  // Different parents are not equal
  else
  {
    return false;
  }
}

template<typename XToType>
bool ChildrenAs<XToType>::visit(
  const std::function<bool(const typename ChildrenAs<XToType>::ToType&)>& fn) const
{
  auto resource = static_pointer_cast<Resource>(m_from.resource());
  auto session = resource->session();
  TopoDS_Iterator it;
  for (it.Initialize(*m_from.data()); it.More(); it.Next())
  {
    auto uid = session->findID(it.Value());
    if (!uid)
    {
      continue;
    }
    auto node = dynamic_cast<ToType*>(resource->find(uid).get());
    if (!node)
    {
      continue;
    }
    if (fn(*node))
    {
      return true;
    }
  }
  return false;
}

template<typename XToType>
bool ChildrenAs<XToType>::visit(
  const std::function<bool(typename ChildrenAs<XToType>::ToType&)>& fn)
{
  auto resource = static_pointer_cast<Resource>(m_from.resource());
  auto session = resource->session();
  TopoDS_Iterator it;
  for (it.Initialize(*m_from.data()); it.More(); it.Next())
  {
    auto uid = session->findID(it.Value());
    if (!uid)
    {
      continue;
    }
    auto node = dynamic_cast<ToType*>(resource->find(uid).get());
    if (!node)
    {
      continue;
    }
    if (fn(*node))
    {
      return true;
    }
  }
  return false;
}
}
}
}

#endif
