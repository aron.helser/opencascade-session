// Testing Environment
#include "SessionEnv.h"

// Session types
#include "smtk/session/opencascade/Resource.h"
#include "smtk/session/opencascade/Session.h"
#include "smtk/session/opencascade/arcs/ChildrenAs.txx"
#include "smtk/session/opencascade/arcs/ParentsAs.txx"

// Session operators
#include "smtk/session/opencascade/operators/Import.h"
#include "smtk/session/opencascade/operators/Read.h"
#include "smtk/session/opencascade/operators/Write.h"

// Test
#include "smtk/common/testing/cxx/helpers.h"

// SMTK Tools
#include "smtk/resource/Manager.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"

#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/ResourceItem.h"

// Thirdparty
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;
namespace ocs = smtk::session::opencascade;
namespace
{

fs::path dataRoot = DATA_DIR;
fs::path testRoot = SCRATCH_DIR;

template<typename Visitor>
void VisitChildren(TopoDS_Shape& shape, Visitor visitor)
{
  // NOTE: The usage of this API requires the use of the TopoDS_Iterator in order
  // to guarentee an ordering that is consistent with the OCCT backend
  TopoDS_Iterator it;
  for (it.Initialize(shape); it.More(); it.Next())
  {
    auto subshape = it.Value();
    if (visitor(subshape))
    {
      break;
    }
  }
};

const int OP_SUCCEEDED = (int) smtk::operation::Operation::Outcome::SUCCEEDED;

}

int TestReadWrite(int, char* [])
{
  testRoot = testRoot / "TestReadWrite";
  if(!fs::exists(testRoot))
  {
    fs::create_directories(testRoot);
  }

  smtk::session::opencascade::SessionEnv env;

  smtk::session::opencascade::Resource::Ptr resource;

  fs::path dataPath = dataRoot / "test.step";
  smtkTest(fs::exists(dataPath),
      "Data file does not exist");

  fs::path smtkPath = testRoot / "test.smtk";

  // Import a resource
  {
    auto op = env.operationManager()->create<smtk::session::opencascade::Import>();
    op->parameters()->findFile("filename")->setValue(dataPath.string());
    auto result = op->operate();
    smtkTest(result->findInt("outcome")->value() == OP_SUCCEEDED,
        "Failed to import test file");

    resource = std::dynamic_pointer_cast<smtk::session::opencascade::Resource>(result->findResource("resource")->value());
  }

  smtk::session::opencascade::Session::Ptr session = resource->session();

  std::unordered_map<smtk::common::UUID,int> shapes;
  // Capture the shapes that make up the compound
  {
    std::function<bool(TopoDS_Shape&)> visitor = [&](TopoDS_Shape& shape) -> bool {
      auto uid = session->findID(shape);
      if (!uid.isNull() && shapes.find(uid) == shapes.end())
      {
        shapes.emplace(uid, shape.ShapeType());
        VisitChildren(shape, visitor);
      }
      // Don't stop
      return false;
    };

    TopoDS_Shape& compound = resource->compound();
    visitor(compound);
  }

  // Write the resource
  {
    auto op = env.operationManager()->create<smtk::session::opencascade::Write>();
    resource->setLocation(smtkPath.string());
    op->parameters()->associate(resource);
    auto result = op->operate();
    smtkTest(result->findInt("outcome")->value() == OP_SUCCEEDED,
        "Failed to write test file");
    smtkTest(fs::exists(smtkPath),
        "Test file does not exsits");

    // Remove the resource after writing
    env.resourceManager()->remove(resource);
    resource.reset();
  }

  // Read the resource and validate the shapes
  {
    auto op = env.operationManager()->create<smtk::session::opencascade::Read>();
    op->parameters()->findFile("filename")->setValue(smtkPath.string());
    auto result = op->operate();
    smtkTest(result->findInt("outcome")->value() == OP_SUCCEEDED,
        "Failed to read test file");
    resource = std::dynamic_pointer_cast<smtk::session::opencascade::Resource>(result->findResource("resource")->value());
  }

  // Check that the shapes match the original
  {
    std::function<bool(TopoDS_Shape&)> visitor = [&](TopoDS_Shape& shape) -> bool {
      auto uid = session->findID(shape);
      if (!uid.isNull())
      {
        auto it = shapes.find(uid);
        smtkTest(it != shapes.end(),
            "Could not find shape id: " << uid);
        smtkTest(it->second == shape.ShapeType(),
            "Shapes do not match, found (" << shape.ShapeType() << ") but expected (" << it->second << ")");
        VisitChildren(shape, visitor);
      }
      // Don't stop
      return false;
    };

    TopoDS_Shape& compound = resource->compound();
    visitor(compound);

    for (auto& node: resource->nodes())
    {
      for(auto& child : std::static_pointer_cast<ocs::Shape>(node)->get<ocs::Children>())
      {
        if (!child.id())
        {
          return 1;
        }
      }
      for(auto& parent: std::static_pointer_cast<ocs::Shape>(node)->get<ocs::Parents>())
      {
        if (!parent.id())
        {
          return 1;
        }
      }
    }
  }

  // If this is not a debug build, and the test succeeded, then clean up afterwards
#ifndef NDEBUG
  fs::remove_all(testRoot);
#endif

  return 0;
}
