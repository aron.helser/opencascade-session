//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/opencascade/operators/Write.h"

#include "smtk/session/opencascade/Write_xml.h"

#include "smtk/session/opencascade/Resource.h"
#include "smtk/session/opencascade/Session.h"
#include "smtk/session/opencascade/json/jsonResource.h"

#include "smtk/operation/MarkGeometry.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ReferenceItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"

#include "smtk/resource/Manager.h"

#include "smtk/io/Logger.h"

#include "smtk/common/Paths.h"

#include "BRepTools.hxx"
#include "BRep_Builder.hxx"
#include "IGESCAFControl_Reader.hxx"
#include "STEPCAFControl_Reader.hxx"
#include "TopoDS_Compound.hxx"
#include "TopoDS_Iterator.hxx"
#include "TopoDS_Shape.hxx"
#include "Transfer_TransientProcess.hxx"
#include "XCAFDoc_DocumentTool.hxx"
#include "XCAFDoc_ShapeTool.hxx"
#include "XSControl_TransferReader.hxx"
#include "XSControl_WorkSession.hxx"

#include <algorithm>
#include <cctype>
#include <mutex>
#include <string>

SMTK_THIRDPARTY_PRE_INCLUDE
#include <boost/filesystem.hpp>
SMTK_THIRDPARTY_POST_INCLUDE

namespace smtk
{
namespace session
{
namespace opencascade
{

Write::Result Write::operateInternal()
{
  smtk::session::opencascade::Resource::Ptr resource;
  smtk::session::opencascade::Session::Ptr session;

  auto result = this->createResult(Write::Outcome::FAILED);

  this->prepareResourceAndSession(result, resource, session);

  std::string location = resource->location();
  if (location.empty())
  {
    smtkErrorMacro(this->log(), "Cannot write session because location not specified");
  }

  nlohmann::json json = resource;

  // Write a BRep file for the compound
  {
    boost::filesystem::path outputFilePath(location);
    boost::filesystem::path brepPath = outputFilePath.replace_extension(".brep");

    json["filename"] = brepPath.string();
    if (!BRepTools::Write(resource->compound(), brepPath.string().c_str()))
    {
      smtkErrorMacro(log(), "Failed to write BRep file");
      return this->createResult(smtk::operation::Operation::Outcome::FAILED);
    }
  }

  if (json.is_null())
  {
    smtkErrorMacro(this->log(), "Unable to serialize OCCT Resource");
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  std::string contents = json.dump(2);
  std::ofstream stream(location);
  stream << contents;
  stream.close();

  result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);

  return result;
}

const char* Write::xmlDescription() const
{
  return Write_xml;
}

bool write(
  const smtk::resource::ResourcePtr& resource,
  const std::shared_ptr<smtk::common::Managers>& managers)
{
  Write::Ptr write = Write::create();
  write->setManagers(managers);
  write->parameters()->associate(resource);
  Write::Result result = write->operate();
  return (result->findInt("outcome")->value() == static_cast<int>(Write::Outcome::SUCCEEDED));
}

} // namespace openscacade
} // namespace session
} // namespace smtk
